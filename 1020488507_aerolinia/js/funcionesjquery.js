$(document).ready(function(){
	$("#btnregistrar").click(function()
    {
    	let validado=0;
	if($("#tipodocumento").val().length == 0){
		$("#validacion_tipodocumento").text("*");
	}
	else{
		$("#validacion_tipodocumento").text("");
		validado++;
	}
	
	
	if($("#documento").val().length == 0){
		$("#validacion_documento").text("*");
		
	}
	else if($("#documento").val().length > 11){
		Swal.fire({
  				icon: 'error',
  				title: 'Lo siento',
  				text: 'El documento no debe superar los 11 dígitos'
			});
	}
	else{
		$("#validacion_documento").text("");
		validado++;
	}

	
	if($("#nombre").val().length == 0){
		$("#validacion_nombre").text("*");
		
	}
	else if($("#nombre").val().length > 30){
		Swal.fire({
  				icon: 'error',
  				title: 'Lo siento',
  				text: 'El nombre no debe superar los 30 dígitos'
			});
	}
	else{
		$("#validacion_nombre").text("");
		validado++;
	}

	//validación del apellido
	if($("#apellido").val().length == 0){
		$("#validacion_apellido").text("*");
		
	}
	else if($("#apellido").val().length > 30){
		Swal.fire({
  				icon: 'error',
  				title: 'Lo siento',
  				text: 'El apellido no debe superar los 30 dígitos'
			});
	}
	else{
		$("#validacion_apellido").text("");
		validado++;
	}

	if($("#origen").val().length == 0){
		$("#validacion_origen").text("*");
	}
	else{
		$("#validacion_origen").text("");
		validado++;
	}

	if($("#destino").val().length == 0){
		$("#validacion_destino").text("*");
	}
	else{
		$("#validacion_destino").text("");
		validado++;
	}

	var fecha_inicial = new Date($("#fecha_inicial").val());
	var fecha_final = new Date($("#fecha_final").val());
	if(fecha_inicial > fecha_final)
	{
		Swal.fire({
  			icon: 'error',
  			title: 'Lo siento',
  			text: 'La fecha de origen debe ser mayor a la fecha de destino'
		});
	}else{
		valido++;
	}

	if($("#pasajeros").val().length == 0){
		$("#validacion_pasajeros").text("*");
	}
	else{
		$("#validacion_pasajeros").text("");
		validado++;
	}

	if(!$("input[name='pago']").is(':checked')){
		Swal.fire({
  			icon: 'error',
  			title: 'Lo siento',
  			text: 'Debe de elegir un metodo de pago'
		});
			
	}

	valor_total();

	if (validado == 8) {
		Swal.fire({
			position: 'top-center',
  			icon: 'success',
  			title: ' WELCOME LauraAero',
  			showConfirmButton: false,
  			timer: 1500
		});			
	}
    });
})

$(document).ready(function() {
    $('.solo_numeros').on("keyup",function(){
    	this.value = this.value.replace(/[^0-9]/g,'')
    });
} );

$(document).ready(function(){
	$("#btnlimpiar").click(function()
    {
    	$("input[name='nombre']").val("");
		$("select").val("");
		$("input[name='pago']").prop('checked',false);
    });
})


function valor_total() {
var valor_unitario = document.getElementById('valor_unitario').value;
var valor_bruto = document.getElementById('valor_bruto').value;
var valor_iva = document.getElementById('valor_iva').value;
var valor_total = document.getElementById('valor_total').value;
var pasajeros = document.getElementById('pasajeros').value;

valor_bruto = valor_unitario * pasajeros;
valor_iva = valor_bruto * 0.19;
valor_total = valor_bruto + valor_iva;

document.getElementById('valor_bruto').value = valor_bruto;
document.getElementById('valor_iva').value = valor_iva;
document.getElementById('valor_total').value = valor_total;
}
